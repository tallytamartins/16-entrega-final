import express from 'express'
import mongoose from 'mongoose'
import pokemonSchema from './mongoose/Schema/pokemonSchema.js'

const app = express()
const db = mongoose.connection

mongoose.connect('mongodb://localhost:27017/tallytamartins', {
  useNewUrlParser: true
})
db.on('error', err => console.log(err))
db.once('open', () => console.log('conected'))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Origin', req.headers.origin)
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization'
  )
  if (req.method === 'OPTIONS') {
    return res.status(200).end()
  } else {
    next()
  }
})

const Pokemon = mongoose.model('pokemon', pokemonSchema)

// create new pokemon
app.post('/new-pokemon', async (req, res) => {
  const { name, imageUrl, evolution } = req.body

  const pokemon = new Pokemon({
    name,
    imageUrl,
    evolution
  })

  try {
    await pokemon.save()
    return res.status(201).json(pokemon)
  } catch (err) {
    console.log(err)
    res.status(400).json({ error: 'cannot post' })
  }
})

// show all pokemons
app.get('/', async (_, res) => {
  try {
    const result = await Pokemon.find().sort({ evolution: 1 })
    if (result.length !== 0) {
      res.status(200).json(result)
    } else {
      res.status(400).json({ error: 'no pokemons found' })
    }
  } catch (err) {
    console.log(err)
    res.status(400).json({ error: 'cannot get' })
  }
})

// edit pokemons
app.put('/update-pokemon/:id', async (req, res) => {
  const { name, imageUrl, evolution } = req.body
  const { id } = req.params

  try {
    await Pokemon.updateOne(
      {
        _id: id
      },
      {
        $set: {
          _id: id,
          name,
          imageUrl,
          evolution
        }
      }
    )

    const result = await Pokemon.findById(id)
    res.status(200).json(result)
  } catch (err) {
    console.log(err)
    res.status(400).json({ error: 'cannot put' })
  }
})

// delet pokemons
app.delete('/delete-pokemon/:id', async (req, res) => {
  const { id } = req.params

  try {
    await Pokemon.deleteOne({
      _id: id
    })

    res.status(204).json()
  } catch (err) {
    console.log(err)
    res.status(400).json({ error: 'cannot delete' })
  }
})

// Page not found
app.use((_, res) => {
  res.status(404).json({ error: 'page not found' })
})

app.listen(4000, () => console.log('app online in localhost:4000'))

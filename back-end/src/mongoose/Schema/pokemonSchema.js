import mongoose from 'mongoose'

const pokemonSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    imageUrl: { type: String, required: true },
    evolution: { type: Number, required: true }
  },
  { versionKey: false }
)

export default pokemonSchema
